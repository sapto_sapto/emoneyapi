package com.sg.emoney.api.emoneyapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class EmoneyapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmoneyapiApplication.class, args);
	}
}
